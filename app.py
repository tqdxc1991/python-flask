from typing import Collection
from flask import Flask, render_template, url_for, flash, redirect
from flask_mongoengine import MongoEngine
import pymongo
from pymongo.mongo_client import MongoClient
from forms import AddForm, RegistrationForm, LoginForm

app = Flask(__name__)
app.config['SECRET_KEY']='e50c8a0787f417205489bd9372ef6f31'

db = MongoEngine()
db.init_app(app)

conn_str = "mongodb+srv://qiong:L6jQWVJR9cjKLQZ@cluster0.qyii0.mongodb.net/note?retryWrites=true&w=majority"
client = pymongo.MongoClient(conn_str)
print(client.server_info())



#cluster = MongoClient(conn_str)
dbb = client["note"]
table = dbb["note"]
nnotes = [
    {
        '_id':0,
        'user':'aurel',
        'content' :'Lorem ipsum dolor sit amet consectetur adipisicing elit.',
        'studentlist':[1,2,3],
        'date':'today' 
    },
    {
        '_id':1,
        'user':'aurel',
        'content' :'Lorem ipsum dolor sit amet consectetur adipisicing elit.',
        'studentlist':[1,2],
        'date':'today' 
    },
        {
        '_id':2,
        'user':'raph',
        'content' :'Lorem ipsum dolor sit amet consectetur adipisicing elit.',
        'studentlist':3,
        'date':'today' 
    },
]

table.insert_one({
        'user':'raph',
        'content' :'test',
        'studentlist':3,
        'date':'today' 
    })
#notes=table.find({})


# class Note(db.Document):
#     note_id = db.In

@app.route("/")
@app.route("/home")
def home():
    notes = table.find()
    student1 = table.find({'studentlist':1})
    student2 = table.find({'studentlist':2})
    student3 = table.find({'studentlist':3})
    tous = table.find({'studentlist':[1,2,3]})
    return render_template('home.html',notes=notes,student1=student1,student2=student2,student3=student3,tous=tous)



@app.route("/register", methods=['GET','POST'])
def register():
    form = RegistrationForm()
    if form.validate_on_submit():
        flash('Account created!','success')
        return redirect(url_for('home'))
    return render_template('register.html',title='Register',form=form)

@app.route("/login", methods=['GET','POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        if form.email.data == 'admin@blog.com' and form.password.data == 'password':
            flash('You have been logged in!','success')
            return redirect(url_for('add'))
        else:
            flash('login failed','danger')
    return render_template('login.html',title='Login',form=form)

@app.route("/add", methods=['GET','POST'])
def add():
    form = AddForm()
    if form.validate_on_submit():
        print("ok")
        newnote = form.newnote.data
        studentlist = form.studentlist.data
        date = form.date.data
        addnew = {
        'user':'raph',
        'content' :newnote,
        'studentlist':studentlist,
        'date':date 
        }
        global table
        table.insert_one(addnew)
        flash('Note added!','success')
        return redirect(url_for('home'))
    else:
        print("pas ok")
    return render_template('add.html',title='Add a note',form=form)




app.run(debug=True)

#select * from student-note   where sn.id_student=10 join note where sn.id_note=note.id
#select * from note join sn where 