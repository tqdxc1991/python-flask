Contexte du projet

Le quotidien - dans les formations Simplon - est riche d'événements qu'il est important de retenir, d'une part afin que les formateurs, qui se relaient, aient une bonne connaissance de la progression de la formation, mais aussi afin d'avoir une vision globale de l'état d'esprit, des difficultés et autres situations individuelles. Quelques exemples :

    compte-rendu d'entretiens individuels
    difficultés personnelles
    temps alloué à un projet inadéquat
    modalités pédagogiques particulièrement efficaces
    informations ou demandes ressorties dans un stand-up meeting
    comportements inappropriés
    retards anormaux
    note à destination du prochain formateur
    etc...

Il est important de différencier les informations collectives de celles qui ne concernent qu'un ou plusieurs individus. Il faut aussi enregistrer l'auteur de la note. Ce projet n'est utilisé que par l'équipe pédagogique. La date de la prise de note est automatiquement pré-renseignée avec le jour courant, mais peut être modifiée en cas de besoin.

Le programme doit être composé de :

    un formulaire de saisie (qui écrit quoi, au sujet de qui, à quelle date ?)
    une page qui permet de consulter le journal
    cette page doit permettre de faire une restriction par apprenant

Bien évidemment, l'interface doit être intuitive, et l'accessibilité qualitative, peut importe le matériel employé.
Modalités pédagogiques

Projet à réaliser individuellement sur trois jours.

Le stack technique sera précisé par votre formateur.
Critères de performance

    C1 : Les maquettes prennent en compte les demandes formulées dans l'expression des besoins.
    C2 : les interfaces sont conformes aux maquettes
    C3 : la sélection d'un apprenant permet de restreindre l'affichage à un seul apprenant
    C5 : le modèle entité/association est fourni et conforme aux attentes
    C6 : les traitements relatifs aux manipulations des données sont conformes aux attentes
    C7 : le programme permet de saisir une note, et de visualiser le journal

Modalités d'évaluation

Publication des livrables, complété d'une démo live au formateur dans le respect des délais impartis. Prenez soin de proposer une heure au formateur pour la démo live à l'avance ! 10 minutes suffisent.
Livrables

Un dépôt GIT, contenant :
- les sources
- les maquettes
- le modèle entité/relation de la BDD (qu'elle soit relationnelle ou NoSQL)